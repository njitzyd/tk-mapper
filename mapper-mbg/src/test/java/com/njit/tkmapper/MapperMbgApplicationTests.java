package com.njit.tkmapper;

import com.njit.tkmapper.entity.Employee;
import com.njit.tkmapper.entity.User;
import com.njit.tkmapper.mappers.UserMapper;
import com.njit.tkmapper.service.EmployeeMapperService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.ArrayList;
import java.util.List;

@SpringBootTest
class MapperMbgApplicationTests {

    @Autowired
    private EmployeeMapperService employeeMapperService;

    @Autowired
    private UserMapper userMapper;

    @Test
    void contextLoads() {
        List<Employee> list = employeeMapperService.selectAll();
        list.forEach(System.out::println);

        list = employeeMapperService.selectAll();
        list.forEach(System.out::println);

    }

    @Test
    void testBatchUpdate() {
        List<Employee> list = new ArrayList<>();
        Employee e1= new Employee(2,"new",400.0,20);
        Employee e2= new Employee(3,"new",400.0,20);
        list.add(e1);
        list.add(e2);
        employeeMapperService.batchUpdate(list);
        list.forEach(System.out::println);
    }

    @Test
    void testHandler(){
        User user = userMapper.selectByPrimaryKey(2);
        System.out.println(user);
    }
}
