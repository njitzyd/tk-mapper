package com.njit.tkmapper;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import tk.mybatis.spring.annotation.MapperScan;

@SpringBootApplication
@MapperScan("com.njit.tkmapper.mappers")
public class MapperMbgApplication {

    public static void main(String[] args) {
        SpringApplication.run(MapperMbgApplication.class, args);
    }

}
