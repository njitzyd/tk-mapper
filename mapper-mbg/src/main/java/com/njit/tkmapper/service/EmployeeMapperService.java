package com.njit.tkmapper.service;

import com.njit.tkmapper.entity.Employee;
import com.njit.tkmapper.mappers.EmployeeMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.common.BaseMapper;

import java.util.List;

/**
 * @ClassName EmployeeMapper
 * @Description TODO
 * @auther zhuyoude
 * @Date 2020/5/20 22:13
 * @Version 1.0
 **/
@Service
public class EmployeeMapperService {

    @Autowired
    private EmployeeMapper employeeMapper;

    public List<Employee> selectAll() {


        return employeeMapper.selectAll();
    }

    /**
     * @Author zhuyoude
     * @Description //测试自己扩展的批量更新
     * @Date  2020/5/21 12:07
     * @Param [list]
     * @return void
     **/
    public void batchUpdate(List<Employee> list) {
        employeeMapper.batchUpdateMapper(list);
    }
}
