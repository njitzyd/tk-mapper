package com.njit.tkmapper.extend;

import tk.mybatis.mapper.annotation.RegisterMapper;

@RegisterMapper
public interface MyMapper<T> extends MyBatchUpdateMapper{
}
