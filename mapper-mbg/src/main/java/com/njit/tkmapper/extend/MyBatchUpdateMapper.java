package com.njit.tkmapper.extend;

import org.apache.ibatis.annotations.UpdateProvider;

import java.util.List;

public interface MyBatchUpdateMapper<T> {

    @UpdateProvider(
            type = MyBatchUpdateProvider.class,
            method = "dynamicSQL")
    void batchUpdateMapper(List<T> list);
}
