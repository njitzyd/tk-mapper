package com.njit.tkmapper.mappers;

import com.njit.tkmapper.entity.User;
import tk.mybatis.mapper.common.Mapper;

public interface UserMapper extends Mapper<User> {
}
