package com.njit.tkmapper.mappers;

import com.njit.tkmapper.entity.Employee;
import com.njit.tkmapper.extend.MyMapper;
import org.apache.ibatis.annotations.CacheNamespace;
import tk.mybatis.mapper.common.Mapper;

@CacheNamespace
public interface EmployeeMapper extends Mapper<Employee>,MyMapper<Employee> {
}