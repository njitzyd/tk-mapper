package com.njit.tkmapper.service;

import com.njit.tkmapper.mappers.EmployeeMapper;
import com.njit.tkmapper.pojo.Employee;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @ClassName EmployeeService
 * @Description TODO
 * @auther zhuyoude
 * @Date 2020/5/20 14:23
 * @Version 1.0
 **/
@Service
public class EmployeeService {

    @Autowired
    private EmployeeMapper employeeMapper;


    public List<Employee> selectList(){
        return employeeMapper.selectAll();
    }

    public void insertEmployee(Employee employee) {

        int insert = employeeMapper.insert(employee);
    }

}

