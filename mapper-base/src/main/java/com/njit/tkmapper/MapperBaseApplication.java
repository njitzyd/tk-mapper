package com.njit.tkmapper;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import tk.mybatis.spring.annotation.MapperScan;

@SpringBootApplication
@MapperScan("com.njit.tkmapper.mappers")
public class MapperBaseApplication {

    public static void main(String[] args) {
        SpringApplication.run(MapperBaseApplication.class, args);
    }

}
