package com.njit.tkmapper;

import com.njit.tkmapper.pojo.Employee;
import com.njit.tkmapper.service.EmployeeService;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

@SpringBootTest
@RunWith(SpringRunner.class)
class MapperBaseApplicationTests {

    @Autowired
    private EmployeeService employeeService;

    // 测试查询所有
    @Test
    void testSelectList(){
        List<Employee> employees = employeeService.selectList();
        employees.forEach(System.out::println);
    }

    @Test
    void testKeySql(){
        Employee employee = new Employee(null,"ttt",3000.0,18);
        employeeService.insertEmployee(employee);
        System.out.println(employee.getEmpId());
    }

}
